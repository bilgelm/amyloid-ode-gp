Requirements:
* R (version 4.0.3 was used for analyses in the paper)
* Deriv package (version 4.1 was used for analyses in the paper)
  (to install this, open R and execute the command: install.packages("Deriv") )

Instructions for running the ODE-GP code on the ADNI data:
* Edit file paths in `ODE_GP_v6_public.R` so the inputs are correctly specified
  - Line 5: edit `dir.name`
  - The R script needs two input files:
    1) `{dir.name}/data/adni/ADNI_BAI_MergedData_Shareable_20200901_wAge.csv`
       For ADNI, this file needs to have the following columns: RID, apoe_group, apoe_cat, apoe_stat, MCSUVRWM, age
    2) `{dir.name}/data/adni/ADNI_FoldsPartitions_20200901_shareable.csv`
        This file specifies the fold number for each visit (for cross-validation).
  - You can further customize these file paths by editing lines 24-25, and some of the column names by editing lines 28-30.
* Create the output directory
  The output directory is set to be
    {dir.name}/output/ODE_GP_v6
  (You can edit this in Line 6.)
  The R script will fail if the output directory doesn't exist, so make sure to create it before running.
* If you don't want to run the 10 cross-validation folds, set Run.folds=F on line 46. 
  Doing so will result in running the algorithm on the entire dataset only. 
  If Run.folds=T, then the algorithm will run on the entire dataset first, and then do the folds.
* The script produces two output files:
  1) {output.dir}/template_adni.csv
  2) {output.dir}/adni_ODE.GP.v6.csv
* To run the code from command line:
    Rscript ODE_GP_v6_public.R
  (You can also source it from RStudio if you prefer.)
* The script will print out a bunch of stuff, for example,
  "Only 1 data-points subject 4"
  and then
  "step.theta = 1 theta = 0.010000 step.rho = 1 rho = NaN minus.log.lik = -4918.586223"
  And then it will get to the folds and print more of this stuff for each fold. There are 10 folds plus the NA fold "fold: NA"
* Most time consuming lines are 361-363.
